import { CartItem } from '../../store/cart/cart.types';

import { 
  CartIconContainer, 
  Image, 
  ItemDetails, 
  Name, 
} from './cart-item.styles';

type CartItemProps = {
  cartItem: CartItem
};

const ACartItem = ({cartItem}: CartItemProps) => {
  const {name, imageUrl, quantity, price} = cartItem;

  return(
    <CartIconContainer>
      <Image src={imageUrl} alt={name} />
      <ItemDetails>
        <Name>{name}</Name>
        <div>{quantity} x ${price}</div>
      </ItemDetails>
    </CartIconContainer>
  );
}

export default ACartItem;