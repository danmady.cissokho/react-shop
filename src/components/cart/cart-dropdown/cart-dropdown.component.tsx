import { useDispatch, useSelector } from 'react-redux';
import {useNavigate} from 'react-router-dom';

import { setIsCartOpen } from '../../../store/cart/cart.action';
import { selectCartItems } from '../../../store/cart/cart.selector';
import Button from "../../button/button.component";

import ACartItem from "../../cart-item/cart-item.component";

import { 
  CartDropDownContainer, 
  CartItemsDiv, 
  EmptyMessage,
} from './cart-dropdown.styles';

const CartDropdown = () => {

  const dispatch = useDispatch();

  const cartItems = useSelector(selectCartItems);

  const navigate = useNavigate();

  const goToCheckoutHandler = () => {
    dispatch(setIsCartOpen(false));
    navigate("checkout");
  };

  return(
    <CartDropDownContainer>
      {
        cartItems.length ? (
          <>
            <CartItemsDiv>
              {
                cartItems.map((item) => (
                  <ACartItem key={item.id} cartItem={item} />
                ))
              }
            </CartItemsDiv>
            <Button onClick={goToCheckoutHandler}>GO TO CHECKOUT</Button>
          </>
        ) : (
          <EmptyMessage>Your cart is empty</EmptyMessage>
        )
      }
    </CartDropDownContainer>
  );
};

export default CartDropdown;