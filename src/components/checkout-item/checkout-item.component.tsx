import { useDispatch, useSelector } from 'react-redux';
import { addItemToCart, clearItemFromCart, removeItemFromCart } from '../../store/cart/cart.action';
import { selectCartItems } from '../../store/cart/cart.selector';
import { CartItem } from '../../store/cart/cart.types';

import { 
  CheckoutItemContainer, 
  ImageContainer,
  Image, 
  Name, 
  Quantity, 
  Arrow, 
  Value, 
  Price, 
  RemoveButton, 
} from './checkout-item.styles';

type CheckoutItemProps = {
  checkoutItem: CartItem
};

const CheckoutItem = ({checkoutItem}: CheckoutItemProps) => {
  const {name, imageUrl, quantity, price} = checkoutItem;

  const dispatch = useDispatch();
  const cartItems = useSelector(selectCartItems);
  const incrementHandler = () => dispatch(addItemToCart(cartItems, checkoutItem));
  const decrementHandler = () => dispatch(removeItemFromCart(cartItems, checkoutItem));
  const removeItemHandler = () => dispatch(clearItemFromCart(cartItems, checkoutItem));

  return(
    <CheckoutItemContainer>
      <ImageContainer>
        <Image src={imageUrl} alt={name} />
      </ImageContainer>
      <Name>
        {name}
      </Name>
      <Quantity>
        <Arrow onClick={decrementHandler}>&#10094;</Arrow>
        <Value>{quantity}</Value>
        <Arrow onClick={incrementHandler}>&#10095;</Arrow>
      </Quantity>
      <Price>
        {price}
      </Price>
      <RemoveButton onClick={removeItemHandler}>&#10005;</RemoveButton>
    </CheckoutItemContainer>
  );
}

export default CheckoutItem;