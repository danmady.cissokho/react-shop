import { FC, Key } from 'react';
import { useNavigate } from "react-router-dom";
import { Category } from '../../store/categories/category.types';

import { 
  DirectoryItemContainer, 
  BackgroundImage, 
  DirectoryItemBodyContainer, 
  DirectoryItemBodyTitle, 
  DirectoryItemBodySubTitle, 
} from './directory-item.styles';

export type DirectoryCategory = {
  id: Key;
  title: string;
  imageUrl: string;
  route: string;
};

type DirectoryitemProps = {
  category: DirectoryCategory
};

const Directoryitem: FC<DirectoryitemProps> = ({category}) => {

  const {imageUrl, title, route} = category;
  const navigate = useNavigate();

  const onNavigateHandler = () => navigate(route);

  return (
    <DirectoryItemContainer onClick={onNavigateHandler}>
      <BackgroundImage backgroundUrl={imageUrl} />
      <DirectoryItemBodyContainer>
        <DirectoryItemBodyTitle>{title}</DirectoryItemBodyTitle>
        <DirectoryItemBodySubTitle>Shop Now</DirectoryItemBodySubTitle>
      </DirectoryItemBodyContainer>
    </DirectoryItemContainer>
  )
}

export default Directoryitem;