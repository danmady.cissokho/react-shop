import Directoryitem from '../directory-item/directory-item.component';
import CATEGORIES_DATA from '../../categories-data';

import { 
  DirectoryContainer, 
} from './directory.styles';

const Directory = () => {
  
  return (
    <DirectoryContainer>
      {CATEGORIES_DATA.map((category) => (
        <Directoryitem key={category.id} category={category}></Directoryitem>
      ))};
    </DirectoryContainer>
  );
};

export default Directory;
