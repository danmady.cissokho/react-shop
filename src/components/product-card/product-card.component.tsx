import { useDispatch, useSelector } from 'react-redux';
import { selectCartItems } from '../../store/cart/cart.selector';
import { addItemToCart } from '../../store/cart/cart.action';

import { BUTTON_TYPE_CLASSES } from "../button/button.component";

import { CategoryItem } from '../../store/categories/category.types';

import { 
  ProductCardContainer, 
  StyledButton,
  Image, 
  Footer, 
  Name, 
  Price, 
} from './product-card.styles';

type ProductCardProps = {
  product: CategoryItem;
}

const ProductCard = ({product}: ProductCardProps) => {
  const {name, imageUrl, price} = product;
  const dispatch = useDispatch();
  const cartItems = useSelector(selectCartItems);

  const addProductToCart = (): void => {
    dispatch(addItemToCart(cartItems, product));
  }

  return(
    <ProductCardContainer>
      <Image src={imageUrl} alt={name} />
      <Footer>
        <Name>{name}</Name>
        <Price>{price}</Price>
      </Footer>
      <StyledButton buttonType={BUTTON_TYPE_CLASSES.google} onClick={addProductToCart}>Add to card</StyledButton>
    </ProductCardContainer>
  );
}

export default ProductCard;