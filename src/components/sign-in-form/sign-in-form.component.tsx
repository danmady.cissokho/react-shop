import { ChangeEvent, FormEvent, useState } from 'react';
import { useDispatch } from 'react-redux';
import { AuthError, AuthErrorCodes } from 'firebase/auth';

import FormInput from '../form-input/form-input.component';
import Button, { BUTTON_TYPE_CLASSES } from '../button/button.component';

import { googleSignInStart , emailSignInStart} from '../../store/user/user.action';

import { 
  SignInContainer, 
  SignInContainerTitle,
  ButtonsContainer, 
} from './sign-in-form.styles';

const defaultFormfields = {
  email: '',
  password: '',
}

const SignInForm = () => {

  const dispatch = useDispatch();

  const [formFields, setFormFields] = useState(defaultFormfields);
  const { email, password } = formFields;

  const resetFormFields = () => {
    setFormFields(defaultFormfields);
  }

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormFields({...formFields, [name]: value})
  };

  const  handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    
    try{
      dispatch(emailSignInStart(email, password));
      resetFormFields();
    } catch(error){
      switch ((error as AuthError).code) {
        case AuthErrorCodes.INVALID_PASSWORD:
          alert("incorrect password for email");
          break;
        default:
          console.log("error");
          break;
      }
    }
  }

  const signInWithGoogle = async () => {
    dispatch(googleSignInStart());
  };

  return(
    <SignInContainer>
      <SignInContainerTitle>I already have an account</SignInContainerTitle>
      <span>Sign in with your email and password</span>
      <form onSubmit={handleSubmit}>
        <FormInput
          label="Email"
          type="email" 
          required 
          onChange={handleChange} 
          name="email" 
          value={email}/>

        <FormInput
          label="Password"
          type="password" 
          required 
          onChange={handleChange} 
          name="password" 
          value={password}/>
        <ButtonsContainer>
          <Button type="submit">Sign In</Button>
          <Button type="button" buttonType={BUTTON_TYPE_CLASSES.google}  onClick={signInWithGoogle}>Google Sign In</Button>
        </ButtonsContainer>
      </form>
    </SignInContainer>
  );
}

export default SignInForm;