import { ChangeEvent, FormEvent, useState } from 'react';

import FormInput from '../form-input/form-input.component';
import Button from '../button/button.component';

import { 
  SignUpContainer, 
  SignUpContainerTitle,
} from './sign-up-form.styles';
import { useDispatch } from 'react-redux';
import { signUpStart } from '../../store/user/user.action';
import { AuthError, AuthErrorCodes } from 'firebase/auth';

const defaultFormfields = {
  displayName: '',
  email: '',
  password: '',
  confirmPassword: '',
}

const SignUpForm = () => {

  const dispatch = useDispatch();
  const [formFields, setFormFields] = useState(defaultFormfields);
  const { displayName, email, password, confirmPassword } = formFields;

  const resetFormFields = () => {
    setFormFields(defaultFormfields);
  }

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormFields({...formFields, [name]: value})
  };

  const  handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if(password !== confirmPassword) {
      alert('passwords are not matching');
      return;
    }
    
    try{
      dispatch(signUpStart(email, password, displayName))
      resetFormFields();
    } catch(error){
      if((error as AuthError).code === AuthErrorCodes.EMAIL_EXISTS){
        alert('cannot create eamil already in use');
      } else {
        console.log("error");
      }
    }
  }

  return(
    <SignUpContainer>
      <SignUpContainerTitle>Don't have an account ?</SignUpContainerTitle>
      <span>Sign up with your email and password</span>
      <form onSubmit={handleSubmit}>
        <FormInput
          label="Display Name"
          type="text" 
          required 
          onChange={handleChange} 
          name="displayName" 
          value={displayName}/>

        <FormInput
          label="Email"
          type="email" 
          required 
          onChange={handleChange} 
          name="email" 
          value={email}/>

        <FormInput
          label="Password"
          type="password" 
          required 
          onChange={handleChange} 
          name="password" 
          value={password}/>

        <FormInput
          label="Confirm Password"
          type="password" 
          required 
          onChange={handleChange} 
          name="confirmPassword" 
          value={confirmPassword}/>

        <Button type="submit">Sign Up</Button>
      </form>
    </SignUpContainer>
  );
}

export default SignUpForm;