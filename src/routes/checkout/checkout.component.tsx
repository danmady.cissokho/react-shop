import { useSelector } from 'react-redux';
import { selectCartItems, selectCartTotal } from '../../store/cart/cart.selector';
import CheckoutItem from "../../components/checkout-item/checkout-item.component";

import PaymentForm from '../../components/payment-form/payment-form.component';
import { CartItem } from '../../store/cart/cart.types';

import { 
  CheckoutContainer,
  CheckoutHeader,
  HeaderBlock,
  Total
} from './checkout.styles';

const Checkout = () => {

  const cartItems = useSelector(selectCartItems);
  const cartTotal = useSelector(selectCartTotal);

  return(
    <CheckoutContainer>
      <CheckoutHeader>
        <HeaderBlock>
          <span>Product</span> 
        </HeaderBlock>
        <HeaderBlock>
          <span>Description</span>
        </HeaderBlock>
        <HeaderBlock>
          <span>Quantity</span>
        </HeaderBlock>
        <HeaderBlock>
          <span>Price</span>
        </HeaderBlock>
        <HeaderBlock>
          <span>Remove</span>
        </HeaderBlock>
      </CheckoutHeader>
      {cartItems.map((item: CartItem) => (
        <CheckoutItem key={item.id} checkoutItem={item}></CheckoutItem>
      ))}
      <Total>Total: ${cartTotal}</Total>
      <PaymentForm />
    </CheckoutContainer>
  );
}

export default Checkout;